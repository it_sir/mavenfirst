package com.telllhow;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tellhow.eomp.common.UserFactory;
import com.tellhow.eomp.globalvariable.GlobalVariableServlet;

/**
 * 全局变量类，用于在表单视图配置中提供供全局变量。
 * 全局变量类名和方法在/src/main/resources/globalVariable.xml文件中被引用。
 * 
 */
public class GlobalPreference implements GlobalVariableServlet {
	private HttpServletRequest request;

	public void initServletParam(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
	}

	public String getID() {
		return "id";
	}

	public String getUUID() {
		return UUID.randomUUID().toString();
	}

	public String getAppUrl() {
		return request.getContextPath();
	}

	public String getProcessId() {
		return "";
	}

	public String getCurrentEmployeeID() {
		User user = UserFactory.getUserProvider().getUser(User.class);
		return user.getEmployeeid();
	}

	public String getCurrentEmployeeName() {
		User user = UserFactory.getUserProvider().getUser(User.class);
		return user.getUseralias();
	}

	public String getCurrentTimestamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date());
	}
}
