package com.telllhow;

import com.tellhow.eomp.common.Userbase;

/**
 * 用户实体类，可以根据需要扩充用户类的属性。
 * 
 */
public class User extends Userbase{
	
	public User() {
		super();
	}
	/**
	 * 平台固定字段 userid, username, useralias在Userbase中实现
	 */
	
	public String userid;
	public String username;
	public String useralias;
	public String organid;
	public boolean isAdmin;
	public String employeeid;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUseralias() {
		return useralias;
	}

	public void setUseralias(String useralias) {
		this.useralias = useralias;
	}

	public String getOrganid() {
		return organid;
	}

	public void setOrganid(String organid) {
		this.organid = organid;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}
}
