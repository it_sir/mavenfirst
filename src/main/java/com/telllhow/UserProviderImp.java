package com.telllhow;

import net.sf.json.JSONObject;

import org.jasig.cas.client.authentication.AttributePrincipal;

import com.tellhow.eomp.common.IUser;
import com.tellhow.eomp.common.UserProvider;
import com.tellhow.eomp.util.WebContext;

/**
 * 获取用户对象类
 *
 */
public class UserProviderImp implements UserProvider {
	
	public  <T extends IUser>T getUser(Class<T> c) {
		AttributePrincipal principal = (AttributePrincipal) WebContext.getRequest().getUserPrincipal();
		String userJson = JSONObject.fromObject(principal.getAttributes()).toString();
		JSONObject jsonObject=JSONObject.fromObject(userJson);
		IUser user = (IUser) JSONObject.toBean(jsonObject, c);
		return (T) user;
	}
	
	
}
